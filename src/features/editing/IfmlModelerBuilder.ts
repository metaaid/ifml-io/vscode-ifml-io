"use strict";

export class IfmlModelerBuilder {
  contents: string;
  resources: any;

  public constructor(contents: string, resources: any) {
    this.contents = contents;
    this.resources = resources;
  }

  private removeNewLines(contents: string): string {
    return contents.replace(/(\r\n|\n|\r)/gm, " ");
  }

  public buildModelerView(webview: any): string {
    this.contents = this.removeNewLines(this.contents);

    const head = `<!DOCTYPE html>
      <html>
        <head>
          <meta charset="UTF-8" />

          <title>IFML Modeler</title>

          <meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src ${webview.cspSource} https:; script-src ${webview.cspSource} 'nonce-${this.resources.nonce}'; style-src ${webview.cspSource} 'unsafe-inline'; font-src ${webview.cspSource} 'unsafe-inline';" />


          <!-- modeler distro -->
          <script src="${this.resources.js}"></script>

          <!-- required modeler styles -->
          <link rel="stylesheet" nonce="${this.resources.nonce}" href="${this.resources.css}">
          <link rel="stylesheet" nonce="${this.resources.nonce}" href="${this.resources.css2}">
          <link rel="stylesheet" nonce="${this.resources.nonce}" href="${this.resources.css3}">
          <link rel="stylesheet" nonce="${this.resources.nonce}" href="${this.resources.css4}">
        </head>`;

    const body = `
      <body>
        <div class="content">
          <div id="canvas"></div>
        </div>

        <div class="buttons">
          <div class="icon codicon codicon-save" onclick="saveChanges()"></div>
          <div class="spinner"></div>
        </div>

        <script nonce="${this.resources.nonce}">

          const vscode = acquireVsCodeApi();

          // (1) persist web view state
          vscode.setState({ resourcePath: '${this.resources.resourceUri}'});

          // (2) react on messages from outside
          window.addEventListener('message', (event) => {
            const message = event.data;

            switch(message) {
              case 'saveFile': saveChanges(); break;
            }
          })

          // (3) bootstrap modeler instance
          const ifmlModeler = new IfmlJS({
            container: '#canvas',
            keyboard: { bindTo: document }
          });

          keyboardBindings();

          /**
           * Open diagram in our modeler instance.
           *
           * @param {String} ifmlXML diagram to display
           */
          async function openDiagram(ifmlXML) {

            // import diagram
            try {
              await ifmlModeler.importXML(ifmlXML);
            } catch (err) {
              const {
                warnings
              } = err;

              return console.error('could not import IFML diagram', err, warning);
            }
          }

          async function saveDiagramChanges() {
            try {
              const {
                xml
              } = await ifmlModeler.saveXML({ format: true });

              return vscode.postMessage({
                command: 'saveContent',
                content: xml
              });
            } catch (err) {
              return console.error('could not save IFML diagram', err);
            }
          }

          async function saveChanges() {
            const spinner = document.getElementsByClassName("spinner")[0];
            spinner.classList.add("active");

            const saveIcon = document.getElementsByClassName("codicon-save")[0];
            saveIcon.classList.add("hidden");

            await saveDiagramChanges()

            setTimeout(function() {
              spinner.classList.remove("active");
              saveIcon.classList.remove("hidden");
            }, 1000);
          }

          function keyboardBindings() {
            const keyboard = ifmlModeler.get('keyboard');

            keyboard.addListener(function(context) {

              const event = context.keyEvent;

              if (keyboard.isKey(['s', 'S'], event) && keyboard.isCmd(event)) {
                saveChanges();
                return true;
              }
            });
          }

          // open diagram
          openDiagram('${this.contents}');
        </script>
      </body>
    `;

    const tail = ["</html>"].join("\n");

    return head + body + tail;
  }
}
