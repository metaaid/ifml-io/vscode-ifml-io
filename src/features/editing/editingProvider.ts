"use strict";
import * as vscode from "vscode";
import * as path from "path";

import { IfmlModelerBuilder } from "./IfmlModelerBuilder";

const fs = require("fs");

export class EditingProvider {
  public constructor(private _context: vscode.ExtensionContext) {}

  private getUri(webview: vscode.Webview, ...p: string[]): vscode.Uri {
    const fileUri = vscode.Uri.file(
      path.join(this._context.extensionPath, ...p)
    );

    return webview.asWebviewUri(fileUri);
  }

  private getNonce() {
    let text = "";
    const possible =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 32; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  // private getUriFromModule(
  //   webview: vscode.Webview,
  //   module: string,
  //   ...p: string[]
  // ): vscode.Uri {
  //   var modulePath = require.resolve(ifmljs);
  //   var fromPath = path.join(modulePath, ...p);

  //   var fileUri = vscode.Uri.file(fromPath);

  //   return webview.asWebviewUri(fileUri);
  // }

  public provideTextDocumentContent(
    localResource: vscode.Uri,
    webview: vscode.Webview
  ): string {
    const localDocumentPath = localResource.fsPath;

    const contents = fs.readFileSync(localDocumentPath, { encoding: "utf8" });

    const builder = new IfmlModelerBuilder(contents, {
      css: this.getUri(webview, "dist/css/ifml-font.css"),
      css2: this.getUri(webview, "dist/css/diagram-js.css"),
      css3: this.getUri(webview, "dist/css/app.css"),
      css4: this.getUri(webview, "dist/css/modeler.css"),
      js: this.getUri(webview, "dist/ifml-modeler.prod.js"),
      nonce: this.getNonce(),
      // css: webview.asWebviewUri("style.css"),
      // js: webview.asWebviewUri("main.js"),
      // modelerStyles: this.getUri(webview, "out", "assets", "modeler.css"),
      // codiconsFont: this.getUri(
      //   webview,
      //   "node_modules",
      //   "@vscode/codicons",
      //   "dist",
      //   "codicon.css"
      // ),
      // node_modules/@vscode/codicons/dist/codicon.css
      // ifmlFont2: this.getUri(
      //   webview,
      //   "node_modules",
      //   "dist",
      //   "css",
      //   "ifml-font.css"
      // ),
      resourceUri: localResource,
    });

    return builder.buildModelerView(webview);
  }
}
