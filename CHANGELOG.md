# Changelog

All notable changes to the ifml-io vs-code extension will be documented in this file.

## Unreleased Changes

## 0.1.0

* Initial release
