// @ts-check

'use strict';

const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');
const outputPath = 'dist';
const PnpWebpackPlugin = require(`pnp-webpack-plugin`);
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const devMode = process.env.NODE_ENV !== "production";

var assetPath = '.';
// var assetPathFont = assetPath;
var assetPathFont = assetPath + '/font';
var assetPathCss = assetPath + '/css';

//@ts-check
/** @typedef {import('webpack').Configuration} WebpackConfig **/

/** @type WebpackConfig */
const extensionConfig = {
  target: 'node', // vscode extensions run in a Node.js-context 📖 -> https://webpack.js.org/configuration/node/
  entry: './src/extension.ts', // the entry point of this extension, 📖 -> https://webpack.js.org/configuration/entry-context/
  output: {
    // the bundle is stored in the 'dist' folder (check package.json), 📖 -> https://webpack.js.org/configuration/output/
    path: path.resolve(__dirname, outputPath),
    // filename: '[name]',
    filename: 'extension.js',
    libraryTarget: 'commonjs2'
  },
  externals: {
    vscode: 'commonjs vscode' // the vscode-module is created on-the-fly and must be excluded. Add other modules that cannot be webpack'ed, 📖 -> https://webpack.js.org/configuration/externals/
  },

  resolveLoader: {
    plugins: [PnpWebpackPlugin.moduleLoader(module), ],
  },
  resolve: {
    // support reading TypeScript and JavaScript files, 📖 -> https://github.com/TypeStrong/ts-loader
    extensions: ['.ts', '.js'],
    plugins: [PnpWebpackPlugin, ],
  },
  module: {
    rules: [{
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [{
          loader: 'ts-loader',
          options: PnpWebpackPlugin.tsLoaderOptions(),
        }]
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ]
  },
  devtool: 'nosources-source-map',
  infrastructureLogging: {
    level: "log", // enables logging required for problem matchers
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [{
          from: require.resolve("@vscode/codicons/dist/codicon.svg"),
          to: assetPath,
        },
        {
          from: require.resolve("ifml-js/dist/assets/ifml-font/font/ifml-font.eot"),
          to: assetPathFont,
        },
        {
          from: require.resolve("ifml-js/dist/assets/ifml-font/font/ifml-font.svg"),
          to: assetPathFont,
        },
        {
          from: require.resolve("ifml-js/dist/assets/ifml-font/font/ifml-font.ttf"),
          to: assetPathFont,
        },
        {
          from: require.resolve("ifml-js/dist/assets/ifml-font/font/ifml-font.woff"),
          to: assetPathFont,
        },
        {
          from: require.resolve("ifml-js/dist/assets/ifml-font/font/ifml-font.woff2"),
          to: assetPathFont,
        },
        {
          from: require.resolve("ifml-js/dist/assets/ifml-font/css/ifml-font.css"),
          to: assetPathCss,
        },
        {
          from: require.resolve("ifml-js/dist/assets/app.css"),
          to: assetPathCss,
        },
        {
          from: require.resolve("ifml-js/dist/assets/diagram-js.css"),
          to: assetPathCss,
        },
        {
          from: require.resolve("ifml-js/dist/ifml-modeler.production.min.js"),
          to: assetPath + "/ifml-modeler.prod.js",
        },
        {
          from: "src/assets/modeler.css",
          to: assetPathCss,
        },
      ]
    }),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: devMode ? "[name].css" : "[name].[contenthash].css",
      chunkFilename: devMode ? "[id].css" : "[id].[contenthash].css",
    }),
  ],
};
module.exports = [extensionConfig];
