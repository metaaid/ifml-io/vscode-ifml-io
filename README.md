# vs-code-ifml-io

Based on [vs-code-bpmn-io](https://github.com/bpmn-io/vs-code-bpmn-io/)

Display and edit IFML diagrams in VS Code.

## Features

* Open IFML (`.ifml`) in a Modeler to make changes to your diagrams
  * From the editor toolbar
  * Via the command palette ("Open IFML Modeler")
  * Via keyboard shortcut (`CTRL/CMD + SHIFT + V`)
* Save changes to your local file

![alt](./resources/editor-screenshot.png?raw=true)

## How to get it

Type `vs-code-ifml-io` in the Extensions section and directly install it. You can also download it in the [Visual Studio Code Marketplace](https://marketplace.visualstudio.com/items?itemName=metaaid.vs-code-ifml-io) or [setup it locally](#development-setup). 


## Development Setup

First step, clone this project to your local machine.

```sh
$ git clone https://gitlab.com/metaaid/ifml-io/ifml-js.git
$ cd ./vs-code-ifml-io
$ yarn install
$ code .
```

Press `F5` to load and debug the extension in a new VS Code instance.

To execute the test suite simply use

```bash
yarn test
```

The extension integration tests can also be executed from VS Code itself, simple choose the *Extension Tests* in the Debug mode.

## Go further

* Get a [Quickstart](./docs/DEVELOPMENT_QUICKSTART.md) on how to develop VS Code extensions
* Learn how to [release a new version](./docs/RELEASING.md)

## License

MIT
